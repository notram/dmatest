- [About this repo and about me](#about-this-repo-and-about-me)
- [Overview](#overview)
  * [Development workflow on both sides SW and HW](#development-workflow-on-both-sides-sw-and-hw)
  * [Development on the SW side, afer the hardware is ready -the bitstream is generated-](#development-on-the-sw-side--afer-the-hardware-is-ready--the-bitstream-is-generated-)
  * [Input files / Output files and their dependency](#input-files---output-files-and-their-dependency)
    + [Bitstream](#bitstream)
    + [FSBL](#fsbl)
    + [U-boot (the not really needed second stage boot loader)](#u-boot--the-not-really-needed-second-stage-boot-loader-)
    + [Linux kernel](#linux-kernel)
    + [The ramdisk](#the-ramdisk)
    + [The device tree](#the-device-tree)
      - [Last note:](#last-note-)
- [Requiered tools](#requiered-tools)
- [Cloning this repo](#cloning-this-repo)
- [Overview](#overview-1)
- [Creating the hardware](#creating-the-hardware)
  * [Outputs of this step](#outputs-of-this-step)
- [First Stage Boot Loader](#first-stage-boot-loader)
  * [Generating the FSBL](#generating-the-fsbl)
  * [Compiling the FSBL](#compiling-the-fsbl)
  * [Debugging the FSBL](#debugging-the-fsbl)
- [Building the device tree compiler](#building-the-device-tree-compiler)
- [Generating and building the device tree](#generating-and-building-the-device-tree)
- [Building second stage bootloader (u-boot)](#building-second-stage-bootloader--u-boot-)
- [Building the linux kernel and the kernel modules](#building-the-linux-kernel-and-the-kernel-modules)
- [Installing the modules onto the ramdisk](#installing-the-modules-onto-the-ramdisk)
    + [Extract and mount the ramdisk](#extract-and-mount-the-ramdisk)
    + [Install the modules](#install-the-modules)
    + [Unmount compress and pack up the ramdisk](#unmount-compress-and-pack-up-the-ramdisk)
- [Downloading everything via jtag to the device and running it](#downloading-everything-via-jtag-to-the-device-and-running-it)
  * [FSBL and the bitstream](#fsbl-and-the-bitstream)
  * [Uboot](#uboot)
  * [Linux kernel devtree and ramdisk](#linux-kernel-devtree-and-ramdisk)
- [References](#references)

# About this repo and about me

I's a small demo project from xilinx where they let us (developers) test their DMA. DMA which is configured into the FPGA. The DMA it's a basic one (not CDMA, VDMA). Can access larger non contingous memory allocated in RAM via a HP AXI Master interface and stream it's content through AXI stream interface. It stream the data out in a FIFO via a MM2S (memory to stream) channel and puts the data back into the ram via its (S2MM) channel. 

This was my first encounter with linux in embedded design. I haven't compiled a linux kernel before, so I'm trying to help my classmates with this "how to" tutorial. 

I assume that you don't get scared seeing a bash terminal. And you have the basic knowledge of executing commands in it. You might have some missing tools or other problems, where you have to solve them yourself. Downloading software, installing them, exporting environment variables and, searching for files and text in files and other sorceries  might come usefull. 

We had a Digilent Zybo Z7 (z7-010) dev board. An older z7, one whitout the PyCam connector. You can adapt the process for your board.

Prior to this course I've completed other courses where we where tought what is na FPGA and how can we configure them using Hardware Descriptive Languages. 

Never forget: "Experience is what you get, when you don't get what you want. And experience is the most valuable thing that you can offer"

With this motto, lets dive in.

# Overview

To see a big picture of the process, I attached two images, a general one from xilinx, and one drawn by me. The latter one is more descriptive for this project, and I hope that it will complement the first one so it will be more understandable.

## Development workflow on both sides SW and HW
![alt text]( images/image.jpeg "Image from xilinx Getting Started (see references for source)")

The image above basically says, that after you installed the requiered Xilinx tools you can start developing the hardware with the IPI(Intellectual Propery Integrator). After that you can decide if you want to use linux or develop your own custom firmware. As we said, we will follow the path were we can use linux. 

## Development on the SW side, afer the hardware is ready -the bitstream is generated-

![alt text](images/linux_compilation_dmatest.png "The files and their dependency")

## Input files / Output files and their dependency

There are a bunch of it. At the beginnig I had no idea why do I need all of it. Some files/repos were required that I didn't understood why. So now I will try to explain some. As usually I do, it will be a bottom up description. When I found out that a file was required to start the thing, then I tried to avoid it until I found out that it can't be avoided. 

So I knew that the DMA can be tested with a kernel module, and I became eager to start it. I wanted no SD card hastle, no BOOT.bin generation, no eclipse based SDK usage (as our prof. said during labs) and so on. 

### Bitstream

This part is clear. This file is loaded onto the FPGA and preconfigures it, this will be the hardware. It can be downloaded via jtag from the PC, fsbl from SD card, or while the linux is running on the PS. The easyest is via JTAG, but we have to use a PC for it. The bitstream is generated using Vivado.

### FSBL
This is a piece of software which gets the dirty preconfiguration work done on the processor. It is a bare metal code. This is the first running thing on the PS. Initializes the, clk's, RAM, timers and other things which were set in the .hdf file. This also can preconfigure the PL(FPGA), and run some intitialization code for it. After finishing its work or stays put inside an infinite loop, or copies the next file from a memory which has to be executed and jumps to it's start address. 

__Note:__ The .hdf file is also generated with Vivado and contains all the register configurations(PL/PS) which was defined by the hardware developers for a successfull startup.

### U-boot (the not really needed second stage boot loader)

Whith this software you can do some fancy stuff easyly. Like loading the kernel from the network via TFTP. Or booting from a USB drive which has a more complicated file system (like exfat or ntfs). Loading things via i2c or spi/qspi. Checking for updates on a remote server. Loading the kernel from an MMC and so on. (You sould check out it's reference manual.) For our lab work, it just boots into the kernel which was in the boot.bin and loaded into the RAM by the FSBL(if I know correctly).


### Linux kernel

We can download it and run __make__ with some default (from xilinx) and manual (adding the dmatest module) configurations. After make finishes, the kernel is baked, but we have no modules installed. How can we insert the modules? Where do we have to insert them? So now we can rebuild the kernel without modular functionality, and get a monolythic kernel... but that is not fun, it's not up to todays possibilities. So  usually, those files can be installed on a SD card. Thats not the best way either. I thought there has to be some way other thant that, otherwise I will have to walk the card from the dev board to the laptop until it wears out if it wont work on the first try. And it wont work on the first try. So I was searching for another way. 

### The ramdisk

At first I was trying to throw this out on the window, but. After I understood that the kernel is a dedad something whitout the ramdisk I started fancying it. This is a file system which contains all the scripts and binaries needed by an embedded system to boot up. It contains the rootfs which is mounted by the kernel after it starts. This is a precious little file system. So this is a place where I can and have to install the built kernel modules. 

This file system can be downloaded from Xilinx. Also this file system can be built up from scratch with busybox and some other things unknown for me until now.) 

The next steps in this field for me are: understanding the runlevels and the purpose of inittab and fstab

### The device tree

This was another thing which needed some time to become accepted in my dependency list. But if you spend some time reading through the files, as I did, you will find out what it contains. After I gathered some understanding, I could compare this file to a microcontroller's (usually) biggest include file. It contains a lot of hardware or physical adresses of the peripheral devices both from the PS and PL. These usually in a microcontroller ar compiled into the executable with macros like #define UART1_base_address #define sysclk_frequency. This is a tree on which linux climbs at the booting phase and initializes every componenet as it was written in their drivers (uses the compatible keyword to match each tree node with its driver). 

#### Last note:
As I can see, there are some steps in the booting process which can be avoided too (mostly as I think uboot), but it was easier for me to do as the Xilinx guide said. Meh... lazy me. My understanding is that basically uboot passes two adresses of two files which are loaded in the ram (device tree and ramdisk) via the first two registers to the kernel an after that jumps to it. But I'm not 100% sure.

# Requiered tools

It was my first encounter with linux kernel compilation, so I decided to document every step so that my classmates or other could get something running on a Xilinx SoC and FPGA.

The used development board is a Zybo Z7 which was available in class.

The softwares I was using during development:

1. Vivado 2019.1
2. "dtc" device tree compiler
3. "arm-linux-gnueabihf-" package for cross-compilation,  this was installed with vivado
4. "arm-none-eabi-" for bare metal compilation. This is also installed with vivado, but I had some trouble wit it. It was a newer version (8.0) and I got some include errors during FSBL compilation.
5. "hsi" to generate the fsbl and the device tree
6. "xsdb" to connect to the target via jtag and download the executables and bitstream
7. "picocom", "minicom" or "putty" to connect to the device via serial console

# Cloning this repo
Commands listed in this readme assume that everything is sourced in your home directory under the dmatest directory. For ease of execution this repository includes subrepositories with fixed releases of the used modules. 

I want to mention that, this repo whith all it's submodules requires some amount of disk space. Cloning requires some time
```
5.2M	./device-tree-xlnx
5.9M	./dmatest_hw
5.1M	./dtc
8.8M	./fsbl
3.1G	./linux-xlnx
4.0K	./README.md
180M	./u-boot-xlnx
```


If you already cloned this repository and missed that it contains sub repositories then run theese commands too:
```bash
git submodules init
git submodules update
```
if you havent downloaded this before, run this command:
```
git clone --recursive https://gitlab.com/notram/dmatest.git
```
# Overview

# Creating the hardware 
For this step, i give you theree different ways:

1. Skip this step and use the files generated by me
2. Open my project look around and then generate the requirered files
3. Build up from scratch

If you chose option 2 or 3 then check out the README located in the hardware folder. (this is not yet created)


## Outputs of this step
- An .hdf file
- The bitstream for the FPGA



The .hdf file is shortand for harware handoff, which contains everything needed for the software development team. 

If you are willing to use my files, then those are in the hardware folder. If you generated them, they are located in the vivado project folder `prj_name/prj_name.sdk/`

The following steps assume (as I said) that these files already exists: 
- `~/dmatest/dmatest_hw/dmatest_hw.sdk/design_1_wrapper.bit`
- `~/dmatest/dmatest_hw/dmatest_hw.sdk/design_1_wrapper.hdf`

# First Stage Boot Loader
## Generating the FSBL

In order to generate the fsbl you will need the hdf file and    hsi (Hardware Software Interface). For me hsi is located under the following path:

`which hsi`
```
/opt/Xilinx/SDK/2019.1/bin/hsi
```
Now cd into our project dir and start up `hsi`
```
cd ~/dmatest
hsi
```
in `hsi` execute the following commands, this will generate the first stage bootloader into the `-dir` directory
```
set hw [ open_hw_design dmatest_hw/dmatest_hw.sdk/design_1_wrapper.hdf ]

generate_app -hw $hw -os standalone -proc ps7_cortexa9_0 -app zynq_fsbl -sw fsbl -dir fsbl

exit
```

## Compiling the FSBL
```
cd ~/dmatest/fsbl
make 
```
Now we have the fsbl at ~/dmatest/fsbl/__executable.elf__
## Debugging the FSBL
If you have trouble running it, or you are curious if ran correctly then there are som debug prints in the fsbl which can be activated with some preprocessor defines. 

TODO: describe the fsbl debug 

# Building the device tree compiler

Change your working directory into the dtc source dir and simply run make.
```
cd ~/dmatest/dtc
make
```
Now you will find a dtc binary in this directory which can be called when needed.

# Generating and building the device tree

Create the devtree directory if doesnt exist and open p hsi:
```
cd ~/dmatest
mkdir devtree
hsi
```
In hsi run these commands
```
open_hw_design dmatest_hw/dmatest_hw.sdk/design_1_wrapper.hdf
set_repo_path ~/dmatest/device-tree-xlnx
create_sw_design device-tree -os device_tree -proc ps7_cortexa9_0
generate_target -dir ~/dmatest/devtree
```
If everything went well then the generated device tree is in the devtree folder. The dmatest module requires devtree node to probe to. I suggest to create a __dmatest.dtsi__ file in which you include the references for the dma and its channels:

Here is my `dmatest.dtsi` But note that, the pl.dtsi should contain the same dma names as in this file is adressed else the __dtc__ would fail.
```cpp
/ {
	axidmatest_0: axidmatest@0 {
		compatible="xlnx,axi-dma-test-1.00.a";
		dmas = <&axi_dma_0 0 &axi_dma_0 1>;
		dma-names = "axidma0", "axidma1";
	};
};
```
And include it into the `system-top.dtsi
```cpp
...
/dts-v1/;
#include "zynq-7000.dtsi"
#include "pl.dtsi"
#include "pcw.dtsi"
#include "dmatest.dtsi" //<--this line is added
/ {
	chosen {
		bootargs = "earlycon";
		stdout-path = "serial0:115200n8";
	};
...
```

Now we can build it. In my case the device tree simply called on the `system-top.dtsi` returned with an error that mentioned the include syntax was unknown for it. So I searched and found out that you can run the standard `C` preprocessor on it and that will include every `#include` macro and produce one output.
```bash
cpp -nostdinc -I include -I ~/dmatest/devtree -undef -x assembler-with-cpp  ~/dmatest/devtree/system-top.dts -o ~/dmatest/devtree/preprocessed.dts
```
After this we can call the `dtc` on the preprocessed output.
```
~/dmatest/dtc/dtc -I dts -O dtb -o devtree.dtb preprocessed.dts
```

# Building second stage bootloader (u-boot)

This software is located in a submodule. The folder for this submodule is u-boot-xlnx folder

Export theese environment variables for the compilation process:
```bash
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
```
__cd__ into the u-boot source folder and run make.
```bash
cd ~/dmatest/u-boot-xlnx
make distclean
make zynq_zed_defconfig
make
```

The output is __u-boot.elf__

# Building the linux kernel and the kernel modules

You should have the ARCH and CROSS_COMPILE env vars exported. Those were exported in the previeous step, 
```bash
bash> echo $ARCH
arm
bash> echo $CROSS_COMPILE
arm-linux-gnueabihf-
```
if these echo commands return nothing then export them again
```
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
```
__cd__ into the kernel source directory. Clean up if there are some precompiled files. Load the xilinx_zynq default configuration.
```bash
cd ~/dmatest/linux-xlnx
make mrproper
make xilinx_zynq_defconfig
```

Now in the menuconfig:
```bash
make menuconfig
```
navigate to the dma test module and enable it.
Device Drivers ---> DMA Engine Support ---> Xilinx DMA Engines ---> DMA Test client for AXI DMA
and press __m__ to modularize this feauture. Pressing the left right arrow save the configuration and exit the __menuconfig__.

If you cant find this feature you can search for it by pressing `/` and typing in `XILINX_DMATEST` then you should get an output like:
```
 .config - Linux/arm 4.19.0 Kernel Configuration
 > Search (xilinx_dmatest) ─────────────────────────────────────────────────────────
  ┌─────────────────────────────── Search Results ───────────────────────────────┐
  │ Symbol: XILINX_DMATEST [=n]                                                  │  
  │ Type  : tristate                                                             │  
  │ Prompt: DMA Test client for AXI DMA                                          │  
  │   Location:                                                                  │  
  │     -> Device Drivers                                                        │  
  │       -> DMA Engine support (DMADEVICES [=y])                                │  
  │ (1)     -> Xilinx DMA Engines (XILINX_DMA_ENGINES [=y])                      │  
  │   Defined at drivers/dma/xilinx/Kconfig:14                                   │  
  │   Depends on: DMADEVICES [=y] && XILINX_DMA_ENGINES [=y] && XILINX_DMA [=y]  │  
  │                                                                              │  
  ├──────────────────────────────────────────────────────────────────────( 99%)──┤  
  │                                   < Exit >                                   │  
  └──────────────────────────────────────────────────────────────────────────────┘ 
```
If in the `Depends on` field contains a `=n` then most likely was something missing when the default configuration was loaded or something was misconfigured. Then you can search for the other keywords and find out why is that missing based on its dependency.

Now run make to build the kernel in a format that u-boot likes:
```bash
make UIMAGE_LOADADDR=0x8000 uImage
```

With this command build the kernel modules:
```bash
make modules
```
now we have the modules built but not installed anywhere.
# Installing the modules onto the ramdisk
We are going to use the ramdisk that was created by Xilinx, it includes a minimal setup with busybox for the system to start up. We are going to install the kernel modules onto the ramdisk.

First create a backup copy and a mount point:
```bash
cp ~/dmatest/arm_ramdisk.image.gz ~/dmatest/arm_ramdisk.image.gz_backup
mkdir ramdisk
```

### Extract and mount the ramdisk
```bash
gunzip arm_ramdisk.image.gz
sudo mount arm_ramdisk.image ramdisk
```
### Install the modules

```
cd ~/dmatest/linux-xlnx
sudo make modules_install INSTALL_MOD_PATH=~/dmatest/ramdisk/lib/modules
```

### Unmount compress and pack up the ramdisk
```
cd ~/dmatest
sudo umount ramdisk
gzip arm_ramdisk.image
~/dmatest/u-boot-xlnx/tools/mkimage -A arm -T ramdisk -C gzip -d ~/dmatest/arm_ramdisk.image.gz ~/dmatest/uramdisk.image.gz
```

The last command wraps the zipped ramdisk with a checksum preferred by uboot, so that it can verify its integrity when loaded into the ram.


# Downloading everything via jtag to the device and running it 
Dont forget to set the jumper on the dev board to JTAG boot mode and do a hardware reset by switching the switch to off and on again.

Before we start the downloading process open up in another terminal the serial port.
I'm using picocom for this purpose:
```
sudo picocom /dev/ttyUSB1 -b115200
```

Now in the main terminal -in which we were working- start up the xsdb
```bask
xsdb
```
At first connect to the tartget, reset the entire system and select as target a CPU core
```
connect 
rst
target 2
```
To list all available targets run `targets` command.
## FSBL and the bitstream

```
fpga ~/dmatest/dmatest_hw/dmatest_hw.sdk/design_1_wrapper.bit
dow ~/dmatest/fsbl/executable.elf
con
```
now the bitstream and the fsbl is downloaded and with the con command the fsbl is executed. 
After the fsbl is executed we have the vital parts of the CPU up and running. Te most inportant for us is now is the RAM and all the clocks for it. The following commands download the uboot, linux and ramdisk to the ram.

## Uboot
After this set of commands the uboot will be downloaded int to the ram and executed. If everything goes well you should catch the autoboot in the serial tarminal by pressing a key when requested.
```
dow ~/dmatest/u-boot-xlnx/u-boot.elf
exec sleep 5
rwr pc 0x04000000
con
```
## Linux kernel devtree and ramdisk
```
dow -data ~/dmatest/linux-xlnx/arch/arm/boot/uImage 0x3000000
dow -data ~/dmatest/uramdisk.image.gz 0x2000000
dow -data linux_dma_test_0/devtree/devtree.dtb 0x2a00000
```
# References
[An overview](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841738/Getting+Started), this was my starting point. 

The [page](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842337/DMA+Drivers+-+Soft+IPs) whith all the DMA soft IPs and their tests.

All the [submodules](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842156/Fetch+Sources) in this git repo are listed on this page. 

[Forum post](https://forums.xilinx.com/t5/Embedded-Linux/AXI-DMA-test-failure/td-p/805245) where they discuss the failing DMA test was also usefull for me.

[Draw.io](https://www.draw.io/) where I created the dependency drawing listed in the beginning.
